# OSO labware

An ontology describing labware for scientific experiments, based on EMMO and EMMOntoPy.

## Features


## Documentation

The Documentation can be found here: [opensourcelab.gitlab.io/labware](opensourcelab.gitlab.io/labware) or [labware.gitlab.io](oso_labware.gitlab.io/)


## Credits

This package was created with Cookiecutter* and the `opensource/templates/cookiecutter-pypackage`* project template.

[Cookiecutter](https://github.com/audreyr/cookiecutter )
[opensource/templates/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) 
